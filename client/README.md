# PharmaLink

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

---

## Installation process

*Step 1:* Run `npm install` in client directory to add node_modules.

---

*Step 2:* Run `ng serve` to start lite server on your local machine (default port will be :4200).

---

*Step 3:* test application status by using `localhost:4200` on browser.


### For More Info

*go to official documentation of [angular]('https://angular.io')*.

